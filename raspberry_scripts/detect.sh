#!/bin/bash

# not sure why model name includes zero-characters - but strip them away
case "$( cat /proc/device-tree/model | tr -d '\0' )" in
	"Raspberry Pi 3"* )
		export MODEL=3
		;;
	"Raspberry Pi 4"* )
		export MODEL=4
#		echo 4
		;;
	* )
		unset MODEL
		exit 1		# exit code here
		;;
esac
		
