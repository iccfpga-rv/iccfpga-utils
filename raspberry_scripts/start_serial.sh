#!/bin/bash

# it needs picocom
# serial must be enabled via raspi-config

sudo picocom --baud 115200 --echo --imap crcrlf /dev/ttyS0
