#!/bin/bash

# more generic variant of flash_core and upload_core

. ./detect.sh

flags=""
(( $MODEL == 4 )) && {
        flags="4"
}

xsvf="$1"

[ ! -f "$xsvf" ] && {
    echo "$xsvf file not found"
    exit 1
}

time sudo xsvftool-gpio -${flags}x "$xsvf"
