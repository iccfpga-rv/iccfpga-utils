#!/bin/bash

. ./detect.sh

# starts openocd debugger for network-debugging
sudo openocd -c "bindto 0.0.0.0" -f interface/raspberry${MODEL}-native-iccfpga-vexriscv.cfg -f target/iccfpga-vexriscv.cfg 
