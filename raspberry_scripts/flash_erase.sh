#!/bin/bash

. ./detect.sh

flags=""
(( $MODEL == 4 )) && {
        flags="4"
}

echo "erasing will take about 3min ..."

time sudo xsvftool-gpio -${flags}x ./flash_erase.xsvf

echo
echo "if successful, perform a power-cycle to load the FPGA from SPI flash"
