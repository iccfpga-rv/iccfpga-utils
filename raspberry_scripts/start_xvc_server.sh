#!/bin/bash

. ./detect.sh

flags=""
(( $MODEL == 4 )) && {
	flags="-4"
}

ip="$( ifconfig | grep 'eth0' -A1 | tail -n1 | awk '{ print $2 }' )"

echo "Virtual Cable Server startet"
echo
echo "To connect vivado to the server use ${ip}:2542"
echo
sudo xvcpi ${flags}
