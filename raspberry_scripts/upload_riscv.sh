#!/bin/bash

. ./detect.sh

fw="$1"

[ -z "$fw" ] && fw="../iccfpga-bin/iccfpga-rv.elf"

sudo openocd -f interface/raspberry${MODEL}-native-iccfpga-vexriscv.cfg -f target/iccfpga-vexriscv.cfg -c "load_image $fw 0x00000000" -c "sleep 500" -c "resume" -c "exit"


