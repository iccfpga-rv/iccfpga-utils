#!/bin/bash

. ./detect.sh

flags=""
(( $MODEL == 4 )) && {
        flags="4"
}

echo "booting from flash ... will take a couple of seconds"

sudo xsvftool-gpio -${flags}s boot_flash.svf

