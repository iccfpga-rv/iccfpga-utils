#!/bin/bash

. ./detect.sh

flags=""
(( $MODEL == 4 )) && {
	flags="4"
}

file="../iccfpga-bin/iccfpga_flash.xsvf"

[[ "$1" ]] && {
        file="$1"
        [ ! -f "$file" ] && {
                echo "file not found: $file"
                exit 1
        }
}


sudo xsvftool-gpio -${flags}x $file
