#!/bin/bash

# also works with symbolic links
INSTALL_ROOT=$( dirname $( readlink -f $0 ) )

#INSTALL_ROOT=$(pwd)

[[ "$( whoami )" == "root" ]] && {
        echo "please run script without sudo"
        exit 1
}

function is_installed {
        { which "$1" > /dev/null ; } && { 
                return 0
        }
        return 1
}

function ask_reinstall {
        is_installed "$1" && {
                echo
                read -p "reinstall $1 (y/n): " ASK
                echo
                [[ "$ASK" == "y" ]] && {
                        return 0
                }
                return 1
        }
        return 0
}

function error {
        echo "error: $1"
        exit 1
}

function install_deps {
        echo "Update source list and installing dependencies ..."
        sudo apt update && sudo apt install -y libcurl4-gnutls-dev libsystemd-dev libmicrohttpd-dev \
            libtool automake libusb-1.0.0-dev texinfo libusb-dev libyaml-dev pkg-config \
            autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison \
            flex texinfo gperf patchutils bc zlib1g-dev cmake libmicrohttpd-dev picocom git vim || error "installing dependencies failed"
        return 0
}

function install_submodules {
        echo "Initialize submodules..."
        git submodule update --init --recursive || error "cloning submodules failes"
        return 0
}

function install_openocd {
        ask_reinstall openocd || return 0
        echo "Installing OpenOCD..."
        cd ${INSTALL_ROOT}/openocd_riscv 
        ./bootstrap && \
        ./configure --enable-bcm2835gpio --enable-ftdi --enable-dummy --enable-sysfsgpi && \
        make && \
        sudo make install || error "installing openocd failed"
        cd -
        return 0
}

function install_xsvflib {
        ask_reinstall xsvftool-gpio || return 0
        echo "Installing xsvflib..."
        cd ${INSTALL_ROOT}/xsvflib-pi 
        make all && \
        sudo make install || error "installing xsvflib failed"
        cd -
        return 0
}

function install_xvcpi {
        ask_reinstall xvcpi || return 0
        echo "Installing Virtual Cable Server..."
        cd ${INSTALL_ROOT}/xvcpi 
        make all && \
        sudo make install || error "installing xvcpi failed"
        cd -
        return 0
}


function add_config {
        # only add if not alread in config.txt
        { grep -q 'risc-v' /boot/config.txt ; } || {
                echo "Change boot/config.txt..."
		sudo bash -c 'echo -e "\n# risc-v reset to pull-up\ngpio=19=pu\n\n# fpga jtag\n# tck\ngpio=12=pn\n# tms\ngpio=16=pn\n# tdi\ngpio=20=pn\n# tdo\ngpio=21=pu\n" >> /boot/config.txt'
        }
        return 0
}


install_deps && \
install_submodules && \
install_openocd && \
install_xsvflib && \
install_xvcpi && \
add_config || error "installation failed"

echo
echo "The device will be rebooted in 10 seconds..."
sleep 10s
sudo reboot -h now

