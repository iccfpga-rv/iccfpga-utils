#!/bin/bash

. ./detect.sh

flags=""
(( $MODEL == 4 )) && {
        flags="4"
}

echo "flashing will take about 7min ..."

file="../iccfpga-bin/iccfpga_spi_flash.xsvf"

[[ "$1" ]] && {
	file="$1"
	[ ! -f "$file" ] && {
		echo "file not found: $file"
		exit 1
	}
}

time sudo xsvftool-gpio -${flags}x $file

echo 
read -p "boot from flash? (Y/n):  " ASK

[[ "$ASK" == "" || "$ASK" == "y" ]] && {
	./boot_flash.sh
}

